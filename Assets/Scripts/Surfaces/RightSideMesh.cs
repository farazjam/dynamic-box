﻿using UnityEngine;

public class RightSideMesh : Surface
{
    protected override void Create()
    {
        gameObject.transform.localPosition = new Vector3(0f,
                                                            gameObject.transform.localPosition.y,
                                                            (Computer.Instance.fWidth / 2 ) * -1f);

        // Create Vector2 vertices
        Vector2[] vertices2D = new Vector2[] {
            new Vector2(transform.position.x + Computer.Instance.fLength/2, transform.position.y - Computer.Instance.fHeight/2),
            new Vector2(transform.position.x + Computer.Instance.fLength/2, transform.position.y + Computer.Instance.fHeight/2),
            new Vector2(transform.position.x - Computer.Instance.fLength/2, transform.position.y + Computer.Instance.fHeight/2),
            new Vector2(transform.position.x - Computer.Instance.fLength/2, transform.position.y - Computer.Instance.fHeight/2),
        };

        // Use the triangulator to get indices for creating triangles
        Triangulator tr = new Triangulator(vertices2D);
        int[] indices = tr.Triangulate();

        // Create the Vector3 vertices
        Vector3[] vertices = new Vector3[vertices2D.Length];
        for (int i = 0; i < vertices.Length; i++)
        {
            vertices[i] = new Vector3(vertices2D[i].x, vertices2D[i].y, 0);
        }

        DrawMesh(this.gameObject, indices, vertices);

        gameObject.transform.localPosition = new Vector3((Computer.Instance.fLength / 2) * -1f,
                                                            gameObject.transform.localPosition.y,
                                                            gameObject.transform.localPosition.z);
        gameObject.transform.localScale = new Vector3(1f, 0.35f, 1f);

    }
}