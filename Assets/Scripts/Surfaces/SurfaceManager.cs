﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SurfaceManager : MonoBehaviour
{
    public static SurfaceManager Instance;
    [SerializeField] private Surface[] surfaces;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    public void Setup()
    {
        for(int i = 0; i < surfaces.Length; i++)
            surfaces[i].Setup();
    }
}
