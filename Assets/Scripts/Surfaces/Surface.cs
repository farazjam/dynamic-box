﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


abstract public class Surface : MonoBehaviour
{
    public void Setup()
    {
        Clear();
        Invoke("Create", 0.1f);
    }

    protected void Clear()
    {
        if (gameObject.GetComponent<MeshRenderer>())
            Destroy(gameObject.GetComponent<MeshRenderer>());

        if (gameObject.GetComponent<MeshFilter>())
            Destroy(gameObject.GetComponent<MeshFilter>());

        if (gameObject.GetComponent<MeshCollider>())
            Destroy(gameObject.GetComponent<MeshCollider>());
    }

    protected abstract void Create();

    protected void DrawMesh(GameObject gObj, int[] indices, Vector3[] vertices)
    {
        // Create the mesh
        Mesh msh = new Mesh();
        msh.vertices = vertices;
        msh.triangles = indices;
        msh.RecalculateNormals();
        msh.RecalculateBounds();

        // Set up game object with mesh;
        if (!gObj.GetComponent<MeshRenderer>())
        {
            gObj.AddComponent(typeof(MeshRenderer));
            gObj.GetComponent<MeshRenderer>().enabled = false;
        }

        if (!gObj.GetComponent<MeshFilter>())
        {
            MeshFilter filter = gObj.AddComponent(typeof(MeshFilter)) as MeshFilter;
            filter.mesh = msh;
        }

        if (!gObj.GetComponent<MeshCollider>())
        {
            gObj.AddComponent(typeof(MeshCollider));
            GetComponent<MeshCollider>().convex = true;
        }
    }

}
