﻿using UnityEngine;

public class FloorMesh : Surface
{
    protected override void Create()
    {
        // Create Vector2 vertices
        Vector2[] vertices2D = new Vector2[] 
        {
            new Vector2(Computer.Instance.FL.transform.position.x, Computer.Instance.FL.transform.position.z),
            new Vector2(Computer.Instance.FR.transform.position.x, Computer.Instance.FR.transform.position.z),
            new Vector2(Computer.Instance.RR.transform.position.x, Computer.Instance.RR.transform.position.z),
            new Vector2(Computer.Instance.RL.transform.position.x, Computer.Instance.RL.transform.position.z),
        };

        // Use the triangulator to get indices for creating triangles
        Triangulator tr = new Triangulator(vertices2D);
        int[] indices = tr.Triangulate();

        // Create the Vector3 vertices
        Vector3[] vertices = new Vector3[vertices2D.Length];
        for (int i = 0; i < vertices.Length; i++)
        {
            vertices[i] = new Vector3(vertices2D[i].x, 0, vertices2D[i].y);
        }

        DrawMesh(this.gameObject, indices, vertices);
    }
}