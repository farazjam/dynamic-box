﻿using UnityEngine;

public class RearSideMesh : Surface
{
    protected override void Create()
    {
        gameObject.transform.localPosition = Vector3.zero;
        transform.rotation = Quaternion.identity;
        transform.Rotate(0, 90, 0);

        Vector2[] vertices2D = new Vector2[] {
            new Vector2(transform.position.x - Computer.Instance.fWidth/2, transform.position.y - Computer.Instance.fHeight/2),
            new Vector2(transform.position.x - Computer.Instance.fWidth/2, transform.position.y + Computer.Instance.fHeight/2),
            new Vector2(transform.position.x + Computer.Instance.fWidth/2, transform.position.y + Computer.Instance.fHeight/2),
            new Vector2(transform.position.x + Computer.Instance.fWidth/2, transform.position.y - Computer.Instance.fHeight/2),
        };

        // Use the triangulator to get indices for creating triangles
        Triangulator tr = new Triangulator(vertices2D);
        int[] indices = tr.Triangulate();

        // Create the Vector3 vertices
        Vector3[] vertices = new Vector3[vertices2D.Length];
        for (int i = 0; i < vertices.Length; i++)
        {
            vertices[i] = new Vector3(vertices2D[i].x, vertices2D[i].y, 0);
        }

        DrawMesh(this.gameObject, indices, vertices);

        gameObject.transform.localPosition = new Vector3(Computer.Instance.fLength * -1f,
                                                            gameObject.transform.localPosition.y,
                                                            gameObject.transform.localPosition.z);
        gameObject.transform.localScale = new Vector3(1f, 0.35f, 1f);
    }
}