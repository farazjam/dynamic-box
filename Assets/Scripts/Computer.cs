﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class Computer : MonoBehaviour
{
    public static Computer Instance;

    public Transform FL;
    public Transform FR;
    public Transform RL;
    public Transform RR;

    public InputField lengthInputField;
    [HideInInspector] public float fLength;
    public InputField widthInputField;
    [HideInInspector] public float fWidth;
    public InputField heightInputField;
    [HideInInspector] public float fHeight;
    [SerializeField] GameObject item;

    private RaycastHit hit;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    void Start()
    {
        fLength = Mathf.Abs(RL.localPosition.x);
        lengthInputField.onEndEdit.AddListener(delegate { NewLengthInput(lengthInputField); });

        fWidth = Mathf.Abs(RL.localPosition.z - RR.localPosition.z);
        widthInputField.onEndEdit.AddListener(delegate { NewWidthInput(widthInputField); });

        fHeight = (float)Math.Round(Mathf.Abs(FL.localScale.y)/100);
        heightInputField.onEndEdit.AddListener(delegate { NewHeightInput(heightInputField); });

        UpdateLength();
        UpdateWidth();
        UpdateHeight();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 10000f, LayerMask.GetMask("Mesh")))
            {
                item.transform.localPosition = hit.point;
                Debug.Log("User clicked " + hit.collider.name);
            }
        }
    }

    void NewLengthInput(InputField input)
    {
        if (input.text.Length > 0)
        {
            float tFLength;
            bool result = float.TryParse(input.text, out tFLength);

            if (result == true && tFLength > 0)
            {
                fLength = (float)Math.Round(tFLength, 2); ;
            }

            UpdateLength();
        }
    }

    void UpdateLength()
    {
        RL.localPosition = new Vector3(fLength * -1, RL.localPosition.y, RL.localPosition.z);
        RR.localPosition = new Vector3(fLength * -1, RR.localPosition.y, RR.localPosition.z);
        lengthInputField.text = fLength.ToString();
        SurfaceManager.Instance.Setup();
    }

    void NewWidthInput(InputField input)
    {
        if (input.text.Length > 0)
        {
            float tFWidth;
            bool result = float.TryParse(input.text, out tFWidth);

            if (result == true && tFWidth > 0)
            {
                fWidth = (float)Math.Round(tFWidth, 2); ;
            }

            UpdateWidth();
        }
    }

    void UpdateWidth()
    {
        RL.localPosition = new Vector3(RL.localPosition.x, RL.localPosition.y, (fWidth / 2) * 1f);
        RR.localPosition = new Vector3(RR.localPosition.x, RR.localPosition.y, (fWidth / 2) * -1f);
        FL.localPosition = new Vector3(FL.localPosition.x, FL.localPosition.y, (fWidth / 2) * 1f);
        FR.localPosition = new Vector3(FR.localPosition.x, FR.localPosition.y, (fWidth / 2) * -1f);
        widthInputField.text = fWidth.ToString();
        SurfaceManager.Instance.Setup();
    }

    void NewHeightInput(InputField input)
    {
        if (input.text.Length > 0)
        {
            float tFHeight;
            bool result = float.TryParse(input.text, out tFHeight);

            if (result == true && tFHeight > 0)
            {
                fHeight = (float)Math.Round(tFHeight, 2); ;
            }

            UpdateHeight();
        }
    }

    void UpdateHeight()
    {
        RL.localScale = new Vector3(RL.localScale.x, fHeight * 100f, RL.localScale.z);
        RR.localScale = new Vector3(RR.localScale.x, fHeight * 100f, RR.localScale.z);
        FL.localScale = new Vector3(FL.localScale.x, fHeight * 100f, FL.localScale.z);
        FR.localScale = new Vector3(FR.localScale.x, fHeight * 100f, FR.localScale.z);
        heightInputField.text = fHeight.ToString();
        SurfaceManager.Instance.Setup();
    }

    void OnGUI()
    {
        GUIStyle labelStyle = new GUIStyle();
        labelStyle.fontSize = 30;
        labelStyle.normal.textColor = Color.white;

        string data = "Length = " + fLength + ", Width = " + fWidth + ", Height = " + fHeight + ", " +
            "Item attached to " + ((hit.collider != null) ? hit.collider.name : "None");
        GUI.Label(new Rect(Screen.width * 0.025f, Screen.height * 0.9f, 300, 65), data, labelStyle);
    }
}
